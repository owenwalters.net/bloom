use diesel::{
    PgConnection,
    r2d2::{PooledConnection, ConnectionManager},
};
use kernel::{
    KernelError,
    events::EventMetadata,
};
use crate::{
    domain::playlist,
    validators,
};


#[derive(Clone, Debug)]
pub struct Rename {
    pub name: String,
    pub metadata: EventMetadata,
}

impl eventsourcing::Command for Rename {
    type Aggregate = playlist::Playlist;
    type Event = playlist::Event;
    type Context = PooledConnection<ConnectionManager<PgConnection>>;
    type Error = KernelError;
    type NonStoredData = ();

    fn validate(&self, _ctx: &Self::Context, _aggregate: &Self::Aggregate) -> Result<(), Self::Error> {
        validators::playlist_name(&self.name)?;

        return Ok(());
    }

    fn build_event(&self, _ctx: &Self::Context, aggregate: &Self::Aggregate) -> Result<(Self::Event, Self::NonStoredData), Self::Error> {
        let data = playlist::EventData::RenamedV1(playlist::RenamedV1{
            name: self.name.clone(),
        });

        return  Ok((playlist::Event{
            id: uuid::Uuid::new_v4(),
            timestamp: chrono::Utc::now(),
            data,
            aggregate_id: aggregate.id,
            metadata: self.metadata.clone(),
        }, ()));
    }
}
