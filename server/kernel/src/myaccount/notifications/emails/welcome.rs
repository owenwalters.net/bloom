use crate::{
    config::Config,
    error::KernelError,
    notifications::emails,
};
use std::collections::BTreeMap;
use handlebars::Handlebars;

static TEMPLATE: &str = r#"
Hey there, <br/>
Welcome to Bloom, a suite of open internet services. <br/>
To get started, <a href='https://bloom.sh/sign-in'>Click Here!</a><br/.
Thanks for joining,<br/>
Bloom"#;


pub fn send_email_welcome(config: &Config, email: &str, recipient_name: &str,
new_email: &str) -> Result<(), KernelError> {

    let handlebars = Handlebars::new();

    let subject = "Welcome to Bloom!";

    let mut data = BTreeMap::new();
    data.insert("new_email".to_string(), new_email.to_string());

    emails::send_email(
        config,
        (emails::NOTIFY_ADDRESS, "Bloom"),
        (email, recipient_name),
        &subject,
        handlebars.render_template(TEMPLATE, &data).expect("error rendering template").as_str(),
    );

    return Ok(());
}
