#[macro_use]
extern crate diesel;

pub mod validators;
pub mod api;
pub mod domain;
pub mod controllers;
