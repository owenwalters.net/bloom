#[macro_use]
extern crate diesel;

pub mod api;
pub mod domain;
pub mod controllers;
pub mod validators;
