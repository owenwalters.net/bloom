# Summary

1. [Architecture](architecture.md)
2. [Development](development.md)
3. [Build](build.md)
4. [Self hosting](self_hosting.md)
